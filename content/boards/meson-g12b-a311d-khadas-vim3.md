---
title: Khadas VIM3 Pro
---

This document provides information needed for adding [Khadas' VIM3 Pro](https://www.khadas.com/vim3) boards to the LAVA lab.

Note that this isn't about the VIM3 Basic board, which has less memory (2GB vs 4GB RAM and 16GB vs. 32GB eMMC).

Specification:
* Amlogic A311D SoC
* 2.2GHz Quad core ARM Cortex-A73 and 1.8GHz dual core Cortex-A53 CPU
* 4GB of RAM
* ARM Mali G52 MP4 GPU 800MHz
* 1 USB 3.0 port (USB-A)
* 1 USB 2.0 port (USB-A)
* 1 USB 2.0 port (USB-C) with power delivery (5-20V input)
* 1 RJ-45 port
* 40-Pin GPIO which includes pins for the SoC UART
* 32GB eMMC

This board is available from [Khadas' store](https://www.khadas.com/product-page/vim3).

### Power control

The VIM3 can be powered via the USB-C port (5-20V input).

It can also be powered via the VIN port on the back of the board, with a
standard 4 circuit, Molex 78172-0004 connector (5-12V input):

![Vim3 bottom interface](/img/vim3_interfaces_bottom.jpg)

### Low-level boot control


- Baud rate: 115200 8n1
- Voltage: 3.3V TTL
- Pins: 17 is GND, 18 is Linux\_RX, 19 is Linux\_TX on the 40 pin extension header

![Serial pinout](/img/vim3_SerialConnections_3Pin.jpg)

#### Network connectivity

The onboard ethernet should work with the vendor U-Boot already flashed.

A random MAC address is generated if there's none defined in the environement. To keep the same MAC address, just do a saveenv in U-Boot.

### Bootloader

The Vim3 boards come with u-boot preflashed which includes networking support
so can be used for lava out of the box.

The bootloader resides in the 16MB SPI-Flash on board (not verified).

These are the memory addresses to load the boot artifacts into:
```
{% set bootm_kernel_addr = '0x01080000' %}
{% set bootm_dtb_addr = '0x01070000' %}
{% set bootm_ramdisk_addr = '0x08000000' %}
```

### Health checks

The board is supported in the upstream Linux kernel starting from at least
linux 5.8. Board dtb is: `amlogic/meson-g12b-a311d-khadas-vim3.dtb`

### Lab notes and trouble shooting

None known.
