---
title: Jacuzzi Chromebooks
---

`jacuzzi` is a board name for arm64 Chromebooks based on the Mediatek
MT8183 SoC. It's an evolution of `kukui`.

The `jacuzzi` Chromebook we're currently using in the lab is an [Acer
Chromebook Spin
311](https://www.acer.com/ac/en/US/content/series/acerchromebookspin311)
(codename `juniper`).

### Debugging interfaces

`jacuzzi` boards have been flashed and tested with [Servo
v4](../../01-debugging_interfaces) interfaces.

The Acer Chromebook Spin 311 supports CCD and can be debugged through
its only USB-C port.

#### Network connectivity

The Servo v4 interface includes an Ethernet interface with a chipset
supported by Depthcharge (R8152).

#### Known issues

No specific issues found for this Chromebook yet, but see [Common
issues](../common_issues.md).

### Example kernel command line arguments

An example of kernel command line arguments to boot a FIT image:

```
earlyprintk=ttyS0,115200n8 console=tty1 console_msg_format=syslog console=ttyS0,115200n8 root=/dev/ram0 ip=dhcp tftpserverip=<server_ip>
```
