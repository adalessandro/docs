---
title: Sarien Chromebooks
---

`sarien` is a board name for x86_64-based Chromebooks. Some examples are:

  - [Dell Latitude 5400 Chromebook Enterprise](https://www.dell.com/en-us/work/shop/dell-laptops-and-notebooks/dell-latitude-5400-chromebook-enterprise/spd/latitude-14-5400-chrome-laptop/xctolc540014us1)
  - [Dell Latitude 5300 2-in-1 Chromebook Enterprise](https://www.dell.com/en-us/work/shop/cty/pdp/spd/latitude-13-5300-2-in-1-chrome-laptop)

The specs for these chromebooks vary in terms of format, display, connectivity
and devices, but they are based on Intel Whiskey Lake 64 bit CPUs
such as the Celeron 4305U.

### Debugging interfaces

The Dell Latitude 5400 Chromebook Enterprise has been flashed and tested
with a [Servo Micro](../../01-debugging_interfaces) interface and a
RTL8152-based USB-Ethernet adapter. Note that these devices don't
support [CCD](../../02-ccd).

#### Network connectivity

An external USB-Ethernet dongle based on the Realtek RTL8152 can be used
to provide Ethernet connectivity in Depthcharge. Alternatively, A Servo v4 can be
used together with the Servo Micro to provide an Ethernet interface,
although this hasn't been tested yet.

#### Known issues

See [Common issues](../common_issues.md).

### Example kernel command line arguments

```
earlyprintk=uart8250,mmio32,0xde000000,115200n8 console_msg_format=syslog console=ttyS0,115200n8 root=/dev/nfs ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug
```

The IP and path of the NFS share are examples and should be adapted to
the test setup.
