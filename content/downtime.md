---
title: LAVA lab downtime management
---

### Planned downtime notice

A notice should be sent to the
[`lava-lab`](mailto:lava-lab@lists.collabora.co.uk) mailing list and posted on
the [`~LAVA`](https://chat.collabora.com/collabora/channels/lava) Mattermost
channel before any planned downtime in the lab.

Please use the template below after updating the start and end times and the
list of device types being impacted:

```
SUBJECT: LAVA lab downtime notice - 2020-09-03
```
```
This is to inform you of some planned downtime in the LAVA lab as
follows:

* Start: 2020-09-03 09:00 BST
* End:   2020-09-03 12:00 BST

* Device types with reduced capacity (min. 2 online):
  * hp-11A-G6-EE-grunt
  * lxc
  * mt8173-elm-hana
  * qemu
  * rk339-gru-kevin

* Device types offline:
  * bcm2836-rpi-2-b
  * hip07-d05
  * imx6q-sabrelite
  * jetson-tk1
  * minnowboard-max-E3825
  * minnowboard-turbot-E3826
  * odroid-xu3
  * peach-pi
  * r8a7796-m3ulcb
  * rk3288-rock2-square
  * rk3288-veyron-jaq
  * sun50i-h6-pine-h64
  * tegra124-nyan-big

* Purpose:
  * <explain why>

Please note that the LAVA Mattermost channel is also available
for support and progress updates:

  https://chat.collabora.com/collabora/channels/lava

Best wishes,
<your name>
```
